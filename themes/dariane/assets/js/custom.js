/*
 * Custom code goes here.
 * A template should always ship with an empty custom.js
 */

jQuery(document).ready(function ($) {

    
    $('#product-acessories .products .product-miniature').addClass('item');
    $('#product-acessories .products.owl-carousel').owlCarousel({
        loop: false,
        margin: 0,
        dots: true,
        nav: true,
        autoplay: false,
        lazyLoad: true,
        navText: '',
        slideBy: 'page',
        responsive: {
            0: {
                items: 1
            },
            576: {
                items: 2
            },
            768: {
                items: 3
            },
            992: {
                items: 4
            }
        }
    });

    $('.category-products.products .product-miniature').addClass('item');
    $('.category-products .products.owl-carousel').owlCarousel({
        loop: false,
        margin: 0,
        dots: true,
        nav: true,
        autoplay: false,
        lazyLoad: true,
        navText: '',
        slideBy: 'page',
        responsive: {
            0: {
                items: 1
            },
            576: {
                items: 2
            },
            768: {
                items: 3
            },
            992: {
                items: 4
            }
        }
    });

    if ($(window).width() <= 991) {
        var cart = $('#_desktop_cart .blockcart');
        var userinfo = $('#_desktop_user_info .user-info');

        $('#_mobile_cart').append(cart);
        $('#_mobile_user_info').append(userinfo);
    }

    // $('#blockcart-modal').on('shown.bs.modal', function(event) {
    // 	console.log('abriu o modal');
    // });

    //ADICIONAR LOADER NO BOTÃO DE COMPRAR
    $('#wrapper').on('click', 'button.add-to-cart', function (event) {
        // console.log($(this));
        if (!$(this).attr('disabled')) {
            $(this).addClass('loading');
        }
    });

    //sobreescrita do ajax do carrinho para atualizar o preview
    prestashop.blockcart = prestashop.blockcart || {};
    // prestashop.blockcart.showModal = function myOwnShowModal (modalHTML) {
    // 	console.log('abriu o modal');
    // 	$('.btn.add-to-cart').removeClass('loading');
    // };

    prestashop.on(
        'updateCart',
        function (event) {
            var refreshURL = $('.blockcart').data('refresh-url');
            var requestData = {};
            if (event && event.reason) {
                requestData = {
                    id_product_attribute: event.reason.idProductAttribute,
                    id_product: event.reason.idProduct,
                    action: event.reason.linkAction
                };
            }
            // console.log(event.reason.cart);  

            $.post(refreshURL, requestData).then(function (resp) {
                $('.blockcart').replaceWith($(resp.preview).find('.blockcart'));
                if (resp.modal) {
                    showModal();
                    var newitem_img = {};
                    var newitem_qty = 0;
                    var newitem_price = '';
                    var newitem_name = '';
                    var newitem_url = '';


                    $.each(event.reason.cart.products, function (index, el) {

                        //verifica se o produto corrent do for é o mesmo que foi recém adicionado no carrinho
                        if (el.id_product == event.reason.idProduct && el.id_product_attribute == event.reason.idProductAttribute) {
                            var url = '';
                            var width = 0;
                            var legend = '';

                            //pega as informações da primeira imagem
                            $.each(el.images, function (index, img) {
                                if (index == 0) {
                                    url = img.small.url;
                                    width = img.small.width;
                                    legend = img.small.legend;
                                }
                            });
                            newitem_img = {
                                url: url,
                                width: width,
                                legend: legend
                            }
                            newitem_qty = el.quantity;
                            newitem_price = el.price;
                            newitem_name = el.name;
                            newitem_url = el.url;
                        }
                    });

                    //prepara o objeto produto para atualizar o preview do carrinho
                    product = {
                        id: event.reason.idProduct,
                        attr: event.reason.idProductAttribute,
                        price: newitem_price,
                        quantity: newitem_qty,
                        name: newitem_name,
                        url: newitem_url,
                        img: {
                            url: newitem_img.url,
                            width: newitem_img.width,
                            legend: newitem_img.legend
                        },
                        remove_url: event.reason.cart.products[0].remove_from_cart_url
                    }

                    // console.log(event.reason.cart);
                    //se tiver o valor do frete, retirar o valor do frete do calculo
                    // para a promoção do frete gratís
                    if(event.reason.cart.subtotals.shipping.amount > 0){
                        cartShipping = event.reason.cart.subtotals.shipping.amount;
                    }
                    else{
                        cartShipping = 0;
                    }

                    totals = {
                        count: event.reason.cart.products_count,
                        value: event.reason.cart.totals.total.value,
                        // valueToFreeShipping: event.reason.cart.subtotals.products.amount - (event.reason.cart.subtotals.discounts.amount != null ? event.reason.cart.subtotals.discounts.amount : 0)
                        valueToFreeShipping: event.reason.cart.totals.total.amount - cartShipping
                    }
                    updateCartPreview(product, totals);
                }
            }).fail(function (resp) {
                prestashop.emit('handleError', {eventType: 'updateShoppingCart', resp: resp});
            });
        }
    );

    //

    //input mask
    $('#sCepDestino').inputmask('99999-999');
    $('input[name="birthday"]').attr('placeholder', 'DD/MM/AAAA');
    $('input[name="birthday"]').inputmask('99/99/9999');
    $('input[name="postcode"]').inputmask('99999-999');

    //customizar tamanho dos banners da home baseado na quantidade
    if ($('#htmlcontent_home .left .item').size() < 3) {
        $('#htmlcontent_home .left .item').eq(1).addClass('fullwidth');
    }



});

function showModal() {
    jQuery('.btn.add-to-cart').removeClass('loading');
}

//função para atualizar o preview do carrinho
function updateCartPreview(product, totals) {
    var currentProducts = jQuery('#cart-preview .product-list .item');
    var isInCart = false;
    var new_item = '';
    var valueToFreeShipping = 0;

      
    jQuery('#cart-preview .cart-total .total-price').text(totals.value);

}
