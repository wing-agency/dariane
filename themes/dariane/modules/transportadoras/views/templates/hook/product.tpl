{if ($product->out_of_stock != 2 || $product->quantity >= 0)}
	<div class="correios-content">
		<span class="control-label">
			{l s="Calcular frete" mod="correios"}
		</span>
		<form method="post" id="calcula_frete_form" action="/modules/transportadoras/calcula-frete.php">
			{*<label for="sCepDestino">{l s="Informe o CEP de destino" mod="correios"}</label>*}
			<input type="hidden" class="form-control" name="product_id" id="product_id" value="{if isset($product->id)}{$product->id}{/if}" required>
			<span class="group">
				<input type="text" class="form-control" name="sCepDestino" id="sCepDestino" placeholder="Informe seu CEP" value="{if isset($sCepDestino)}{$sCepDestino}{/if}" required>
				<button type="submit" class="button btn btn-default button-small">
					<span>Calcular</span>
				</button>
			</span>

			{if $product->additional_shipping_cost|floatval < 0}
				<input type="hidden" name="shipping_cost" id="shipping_cost" value="{$product->additional_shipping_cost|floatval}">
			{/if}
		</form>
		<ul class="unstyled correios_calculos">

		</ul>
	</div>

{/if}