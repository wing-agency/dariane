{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}

<div class="block-contact col-xs-12 col-lg-4 links wrapper">
  <h3 class="block-contact-title">{l s='Store information' d='Shop.Theme.Global'}</h3>
  <div class="content">
      <div class="address">{$contact_infos.address.formatted nofilter}</div>

      <div class="contact-infos">
        <span class="whatsapp">45 99936-0565</span>
        <span class="phone">{$contact_infos.phone}</span>
        <span class="email">
          <a href="mailto:'|cat:$contact_infos.email|cat:'">{$contact_infos.email}</a>
        </span>
      </div>    
      
  </div>
  {*
  <div class="hidden-md-up">
    <div class="title">
      <a class="h3" href="{$urls.pages.stores}">{l s='Store information' d='Shop.Theme.Global'}</a>
    </div>
  </div>
  *}
</div>

<div class="contact-top">
  <div class="whatsapp">
    <span class="fa fa-whatsapp"></span>
    <span class="number">45 99936-0565</span>
  </div>

  {if $contact_infos.phone}
    <div class="phone">
      <span class="fa fa-phone"></span>
      <span class="number">{$contact_infos.phone}</span>
    </div>
  {/if}

  
</div>
