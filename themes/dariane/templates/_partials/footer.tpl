{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
<div class="container">
  <div class="row">
    {block name='hook_footer_before'}
      {hook h='displayFooterBefore'}
    {/block}
  </div>
</div>
<div class="footer-container">
  <div class="container">
    <div class="row">
      {block name='hook_footer'}
        {hook h='displayFooter'}
      {/block}
    </div>
    <div class="row">
      {block name='hook_footer_after'}
        {hook h='displayFooterAfter'}
      {/block}
    </div>
  </div>
</div>
<div class="footer-bottom">
  <div class="container">
    <div class="row seal">
      <div class="payment col-xs-12  col-md-6 col-lg-4">
        <p class="h3 ">Formas de pagamento</p>
        <img src="{$urls.base_url}/themes/dariane/assets/img/payment.jpg">
      </div>
      <div class="security col-xs-6 col-md-3 col-lg-2">
        <p class="h3 ">Segurança</p>
        <img src="{$urls.base_url}/themes/dariane/assets/img/selo.png">
      </div>
      <div class="tech col-xs-6 col-md-3 col-lg-2">
        <p class="h3 ">Tecnologia</p>
        <a href="http://www.agenciawing.com.br" target="_blank">
          <img src="{$urls.base_url}/themes/dariane/assets/img/wing.png" width="60">
        </a>
      </div>
      <div class="col-xs-12 col-lg-4">
        <p class="copy">
          {block name='copyright_link'}
            {*<a class="_blank" href="http://www.prestashop.com" target="_blank">
              {l s='%copyright% %year% - Ecommerce software by %prestashop%' sprintf=['%prestashop%' => 'PrestaShop™', '%year%' => 'Y'|date, '%copyright%' => '©'] d='Shop.Theme.Global'}
            </a>*}
            <span>Dariane Armarinhos ©</span>
            <span>Todos os direitos reservados</span>
            <span>00.000.000/0000-00</span>
            
          {/block}
        </p>
      </div>
    </div>
  </div>
</div>
