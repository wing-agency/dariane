<?php /* Smarty version Smarty-3.1.19, created on 2019-09-06 17:50:37
         compiled from "module:ps_shoppingcart/ps_shoppingcart.tpl" */ ?>
<?php /*%%SmartyHeaderCode:162705d715098b661e2-28324318%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '35655e6409b6198f29dd6e732ef9598dec599880' => 
    array (
      0 => 'module:ps_shoppingcart/ps_shoppingcart.tpl',
      1 => 1567802982,
      2 => 'module',
    ),
  ),
  'nocache_hash' => '162705d715098b661e2-28324318',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5d715098be9e17_42737471',
  'variables' => 
  array (
    'cart' => 0,
    'refresh_url' => 0,
    'cart_url' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d715098be9e17_42737471')) {function content_5d715098be9e17_42737471($_smarty_tpl) {?>
<div id="_desktop_cart" class="">
  <div id="cart-preview" class="blockcart cart-preview <?php if ($_smarty_tpl->tpl_vars['cart']->value['products_count']>0) {?>active<?php } else { ?>inactive<?php }?>" data-refresh-url="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['refresh_url']->value, ENT_QUOTES, 'UTF-8');?>
">
    <div class="header">
      <?php if ($_smarty_tpl->tpl_vars['cart']->value['products_count']>0) {?>
        <a rel="nofollow" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cart_url']->value, ENT_QUOTES, 'UTF-8');?>
">
      <?php } else { ?>
        <span class="inactive-cart">
      <?php }?>
        <span class="icon-bag"></span>
        <span class="cart-label hidden-sm-down">Sua sacola </span>
        

        <strong class="total-price <?php if ($_smarty_tpl->tpl_vars['cart']->value['totals']['total']['amount']<=0) {?>empty<?php }?>"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cart']->value['totals']['total']['value'], ENT_QUOTES, 'UTF-8');?>
</strong>
        
        
      <?php if ($_smarty_tpl->tpl_vars['cart']->value['products_count']>0) {?>
        </a>
      <?php } else { ?>
        </span>
      <?php }?>
    </div>
  </div>
</div>
<?php }} ?>
