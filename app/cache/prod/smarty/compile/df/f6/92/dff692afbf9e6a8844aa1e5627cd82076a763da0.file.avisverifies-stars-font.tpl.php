<?php /* Smarty version Smarty-3.1.19, created on 2019-09-05 21:29:59
         compiled from "C:\wamp64\www\Wing\dariane\modules\netreviews\views\templates\hook\sub\avisverifies-stars-font.tpl" */ ?>
<?php /*%%SmartyHeaderCode:163175d71a88759b8f2-27090179%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'dff692afbf9e6a8844aa1e5627cd82076a763da0' => 
    array (
      0 => 'C:\\wamp64\\www\\Wing\\dariane\\modules\\netreviews\\views\\templates\\hook\\sub\\avisverifies-stars-font.tpl',
      1 => 1567729774,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '163175d71a88759b8f2-27090179',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'av_star_type' => 0,
    'customized_star_color' => 0,
    'average_rate_percent' => 0,
    'review' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5d71a8876f6077_35906073',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d71a8876f6077_35906073')) {function content_5d71a8876f6077_35906073($_smarty_tpl) {?><!--
* 2012-2018 NetReviews
*
*  @author    NetReviews SAS <contact@avis-verifies.com>
*  @copyright 2018 NetReviews SAS
*  @version   Release: $Revision: 7.6.5
*  @license   NetReviews
*  @date      20/09/2018
*  International Registered Trademark & Property of NetReviews SAS
-->
<?php if ($_smarty_tpl->tpl_vars['av_star_type']->value=='big') {?> 
<div class="netreviews_bg_stars_big">
   <div>
      <?php $_smarty_tpl->tpl_vars['av_star'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['av_star']->step = 1;$_smarty_tpl->tpl_vars['av_star']->total = (int) ceil(($_smarty_tpl->tpl_vars['av_star']->step > 0 ? 5+1 - (1) : 1-(5)+1)/abs($_smarty_tpl->tpl_vars['av_star']->step));
if ($_smarty_tpl->tpl_vars['av_star']->total > 0) {
for ($_smarty_tpl->tpl_vars['av_star']->value = 1, $_smarty_tpl->tpl_vars['av_star']->iteration = 1;$_smarty_tpl->tpl_vars['av_star']->iteration <= $_smarty_tpl->tpl_vars['av_star']->total;$_smarty_tpl->tpl_vars['av_star']->value += $_smarty_tpl->tpl_vars['av_star']->step, $_smarty_tpl->tpl_vars['av_star']->iteration++) {
$_smarty_tpl->tpl_vars['av_star']->first = $_smarty_tpl->tpl_vars['av_star']->iteration == 1;$_smarty_tpl->tpl_vars['av_star']->last = $_smarty_tpl->tpl_vars['av_star']->iteration == $_smarty_tpl->tpl_vars['av_star']->total;?><span class="nr-icon nr-star grey"></span><?php }} ?>
   </div>
   <div style="color:#<?php echo $_smarty_tpl->tpl_vars['customized_star_color']->value;?>
">
      <?php $_smarty_tpl->tpl_vars['av_star'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['av_star']->step = 1;$_smarty_tpl->tpl_vars['av_star']->total = (int) ceil(($_smarty_tpl->tpl_vars['av_star']->step > 0 ? $_smarty_tpl->tpl_vars['average_rate_percent']->value['floor']+1 - (0) : 0-($_smarty_tpl->tpl_vars['average_rate_percent']->value['floor'])+1)/abs($_smarty_tpl->tpl_vars['av_star']->step));
if ($_smarty_tpl->tpl_vars['av_star']->total > 0) {
for ($_smarty_tpl->tpl_vars['av_star']->value = 0, $_smarty_tpl->tpl_vars['av_star']->iteration = 1;$_smarty_tpl->tpl_vars['av_star']->iteration <= $_smarty_tpl->tpl_vars['av_star']->total;$_smarty_tpl->tpl_vars['av_star']->value += $_smarty_tpl->tpl_vars['av_star']->step, $_smarty_tpl->tpl_vars['av_star']->iteration++) {
$_smarty_tpl->tpl_vars['av_star']->first = $_smarty_tpl->tpl_vars['av_star']->iteration == 1;$_smarty_tpl->tpl_vars['av_star']->last = $_smarty_tpl->tpl_vars['av_star']->iteration == $_smarty_tpl->tpl_vars['av_star']->total;?><span class="nr-icon nr-star"></span><?php }} ?><?php if ($_smarty_tpl->tpl_vars['average_rate_percent']->value['decimals']) {?><span class="nr-icon nr-star" style="width:<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['average_rate_percent']->value['decimals'],'htmlall','UTF-8');?>
%;"></span><?php }?>
   </div>  
</div> 

<?php } elseif ($_smarty_tpl->tpl_vars['av_star_type']->value=='small') {?> 
<div class="netreviews_font_stars">
    <div>
      <?php $_smarty_tpl->tpl_vars['av_star'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['av_star']->step = 1;$_smarty_tpl->tpl_vars['av_star']->total = (int) ceil(($_smarty_tpl->tpl_vars['av_star']->step > 0 ? 5+1 - (1) : 1-(5)+1)/abs($_smarty_tpl->tpl_vars['av_star']->step));
if ($_smarty_tpl->tpl_vars['av_star']->total > 0) {
for ($_smarty_tpl->tpl_vars['av_star']->value = 1, $_smarty_tpl->tpl_vars['av_star']->iteration = 1;$_smarty_tpl->tpl_vars['av_star']->iteration <= $_smarty_tpl->tpl_vars['av_star']->total;$_smarty_tpl->tpl_vars['av_star']->value += $_smarty_tpl->tpl_vars['av_star']->step, $_smarty_tpl->tpl_vars['av_star']->iteration++) {
$_smarty_tpl->tpl_vars['av_star']->first = $_smarty_tpl->tpl_vars['av_star']->iteration == 1;$_smarty_tpl->tpl_vars['av_star']->last = $_smarty_tpl->tpl_vars['av_star']->iteration == $_smarty_tpl->tpl_vars['av_star']->total;?><span class="nr-icon nr-star grey"></span><?php }} ?>
   </div>
   <div style="color:#<?php echo $_smarty_tpl->tpl_vars['customized_star_color']->value;?>
">
      <?php $_smarty_tpl->tpl_vars['av_star'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['av_star']->step = 1;$_smarty_tpl->tpl_vars['av_star']->total = (int) ceil(($_smarty_tpl->tpl_vars['av_star']->step > 0 ? $_smarty_tpl->tpl_vars['review']->value['rate']+1 - (1) : 1-($_smarty_tpl->tpl_vars['review']->value['rate'])+1)/abs($_smarty_tpl->tpl_vars['av_star']->step));
if ($_smarty_tpl->tpl_vars['av_star']->total > 0) {
for ($_smarty_tpl->tpl_vars['av_star']->value = 1, $_smarty_tpl->tpl_vars['av_star']->iteration = 1;$_smarty_tpl->tpl_vars['av_star']->iteration <= $_smarty_tpl->tpl_vars['av_star']->total;$_smarty_tpl->tpl_vars['av_star']->value += $_smarty_tpl->tpl_vars['av_star']->step, $_smarty_tpl->tpl_vars['av_star']->iteration++) {
$_smarty_tpl->tpl_vars['av_star']->first = $_smarty_tpl->tpl_vars['av_star']->iteration == 1;$_smarty_tpl->tpl_vars['av_star']->last = $_smarty_tpl->tpl_vars['av_star']->iteration == $_smarty_tpl->tpl_vars['av_star']->total;?><span class="nr-icon nr-star"></span><?php }} ?>
   </div> 
</div>

<?php } elseif ($_smarty_tpl->tpl_vars['av_star_type']->value=='widget') {?> 
<div class="netreviews_font_stars">
    <div>
      <?php $_smarty_tpl->tpl_vars['av_star'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['av_star']->step = 1;$_smarty_tpl->tpl_vars['av_star']->total = (int) ceil(($_smarty_tpl->tpl_vars['av_star']->step > 0 ? 5+1 - (1) : 1-(5)+1)/abs($_smarty_tpl->tpl_vars['av_star']->step));
if ($_smarty_tpl->tpl_vars['av_star']->total > 0) {
for ($_smarty_tpl->tpl_vars['av_star']->value = 1, $_smarty_tpl->tpl_vars['av_star']->iteration = 1;$_smarty_tpl->tpl_vars['av_star']->iteration <= $_smarty_tpl->tpl_vars['av_star']->total;$_smarty_tpl->tpl_vars['av_star']->value += $_smarty_tpl->tpl_vars['av_star']->step, $_smarty_tpl->tpl_vars['av_star']->iteration++) {
$_smarty_tpl->tpl_vars['av_star']->first = $_smarty_tpl->tpl_vars['av_star']->iteration == 1;$_smarty_tpl->tpl_vars['av_star']->last = $_smarty_tpl->tpl_vars['av_star']->iteration == $_smarty_tpl->tpl_vars['av_star']->total;?><span class="nr-icon nr-star grey"></span><?php }} ?>
   </div>
   <div style="color:#<?php echo $_smarty_tpl->tpl_vars['customized_star_color']->value;?>
">
      <?php $_smarty_tpl->tpl_vars['av_star'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['av_star']->step = 1;$_smarty_tpl->tpl_vars['av_star']->total = (int) ceil(($_smarty_tpl->tpl_vars['av_star']->step > 0 ? $_smarty_tpl->tpl_vars['average_rate_percent']->value['floor']+1 - (0) : 0-($_smarty_tpl->tpl_vars['average_rate_percent']->value['floor'])+1)/abs($_smarty_tpl->tpl_vars['av_star']->step));
if ($_smarty_tpl->tpl_vars['av_star']->total > 0) {
for ($_smarty_tpl->tpl_vars['av_star']->value = 0, $_smarty_tpl->tpl_vars['av_star']->iteration = 1;$_smarty_tpl->tpl_vars['av_star']->iteration <= $_smarty_tpl->tpl_vars['av_star']->total;$_smarty_tpl->tpl_vars['av_star']->value += $_smarty_tpl->tpl_vars['av_star']->step, $_smarty_tpl->tpl_vars['av_star']->iteration++) {
$_smarty_tpl->tpl_vars['av_star']->first = $_smarty_tpl->tpl_vars['av_star']->iteration == 1;$_smarty_tpl->tpl_vars['av_star']->last = $_smarty_tpl->tpl_vars['av_star']->iteration == $_smarty_tpl->tpl_vars['av_star']->total;?><span class="nr-icon nr-star"></span><?php }} ?><?php if ($_smarty_tpl->tpl_vars['average_rate_percent']->value['decimals']) {?><span class="nr-icon nr-star" style="width:<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['average_rate_percent']->value['decimals'],'htmlall','UTF-8');?>
%;"></span><?php }?>
   </div> 
</div>
 <?php }?>
<?php }} ?>
