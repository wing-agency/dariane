<?php /* Smarty version Smarty-3.1.19, created on 2019-09-06 18:47:11
         compiled from "module:ps_featuredproducts/views/templates/hook/ps_featuredproducts.tpl" */ ?>
<?php /*%%SmartyHeaderCode:248615d71509540ab53-36956159%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fa6cc378d2942c8857b89d6bca728048c0caeedd' => 
    array (
      0 => 'module:ps_featuredproducts/views/templates/hook/ps_featuredproducts.tpl',
      1 => 1567806428,
      2 => 'module',
    ),
  ),
  'nocache_hash' => '248615d71509540ab53-36956159',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5d715095483fb1_53819666',
  'variables' => 
  array (
    'urls' => 0,
    'products' => 0,
    'product' => 0,
    'allProductsLink' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d715095483fb1_53819666')) {function content_5d715095483fb1_53819666($_smarty_tpl) {?>
<section class="featured-products clearfix">
  <h2 class="h2 products-section-title ">
    <?php echo smartyTranslate(array('s'=>'Popular Products','d'=>'Shop.Theme.Catalog'),$_smarty_tpl);?>

  </h2>
  <div class="row products-list">
    <div class="banner-block col-lg-3">
      <a href="#">
        <img class="img-responsive desktop hidden-sm-down" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url'], ENT_QUOTES, 'UTF-8');?>
themes/dariane/assets/img/categoria-desktop.png">
        <img class="img-responsive mobile hidden-sm-up" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url'], ENT_QUOTES, 'UTF-8');?>
themes/dariane/assets/img/categoria-mobile.png">
      </a>
    </div>
    <div class="products  col-lg-9">
      <?php  $_smarty_tpl->tpl_vars["product"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["product"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["product"]->key => $_smarty_tpl->tpl_vars["product"]->value) {
$_smarty_tpl->tpl_vars["product"]->_loop = true;
?>
        <?php echo $_smarty_tpl->getSubTemplate ("catalog/_partials/miniatures/product.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('product'=>$_smarty_tpl->tpl_vars['product']->value), 0);?>

      <?php } ?>
    </div>
  </div>
  <div class="section-footer">
    <a class="all-product-link btn btn-secondary" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['allProductsLink']->value, ENT_QUOTES, 'UTF-8');?>
">
      Ver todos os produtos
      <i class="material-icons">&#xE315;</i>
    </a>
  </div>
</section>
<?php }} ?>
