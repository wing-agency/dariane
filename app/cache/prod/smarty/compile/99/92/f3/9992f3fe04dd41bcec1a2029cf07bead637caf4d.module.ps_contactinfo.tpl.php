<?php /* Smarty version Smarty-3.1.19, created on 2019-09-06 19:47:48
         compiled from "module:ps_contactinfo/ps_contactinfo.tpl" */ ?>
<?php /*%%SmartyHeaderCode:50015d7150987c7e89-77170535%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9992f3fe04dd41bcec1a2029cf07bead637caf4d' => 
    array (
      0 => 'module:ps_contactinfo/ps_contactinfo.tpl',
      1 => 1567810065,
      2 => 'module',
    ),
  ),
  'nocache_hash' => '50015d7150987c7e89-77170535',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5d7150989b25e0_70788635',
  'variables' => 
  array (
    'contact_infos' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d7150989b25e0_70788635')) {function content_5d7150989b25e0_70788635($_smarty_tpl) {?>

<div class="block-contact col-xs-12 col-lg-4 links wrapper">
  <h3 class="block-contact-title"><?php echo smartyTranslate(array('s'=>'Store information','d'=>'Shop.Theme.Global'),$_smarty_tpl);?>
</h3>
  <div class="content">
      <div class="address"><?php echo $_smarty_tpl->tpl_vars['contact_infos']->value['address']['formatted'];?>
</div>

      <div class="contact-infos">
        <span class="whatsapp">45 99936-0565</span>
        <span class="phone"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['contact_infos']->value['phone'], ENT_QUOTES, 'UTF-8');?>
</span>
        <span class="email">
          <a href="mailto:'|cat:$contact_infos.email|cat:'"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['contact_infos']->value['email'], ENT_QUOTES, 'UTF-8');?>
</a>
        </span>
      </div>    
      
  </div>
  
</div>

<div class="contact-top">
  <div class="whatsapp">
    <span class="fa fa-whatsapp"></span>
    <span class="number">45 99936-0565</span>
  </div>

  <?php if ($_smarty_tpl->tpl_vars['contact_infos']->value['phone']) {?>
    <div class="phone">
      <span class="fa fa-phone"></span>
      <span class="number"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['contact_infos']->value['phone'], ENT_QUOTES, 'UTF-8');?>
</span>
    </div>
  <?php }?>

  
</div>
<?php }} ?>
