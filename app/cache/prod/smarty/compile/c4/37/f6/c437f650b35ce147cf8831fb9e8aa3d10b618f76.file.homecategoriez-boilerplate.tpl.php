<?php /* Smarty version Smarty-3.1.19, created on 2019-09-05 19:14:28
         compiled from "C:\wamp64\www\Wing\dariane\themes\dariane\modules\homecategoriez\views\templates\homecategoriez-boilerplate.tpl" */ ?>
<?php /*%%SmartyHeaderCode:201325d71815dcbb028-21592447%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c437f650b35ce147cf8831fb9e8aa3d10b618f76' => 
    array (
      0 => 'C:\\wamp64\\www\\Wing\\dariane\\themes\\dariane\\modules\\homecategoriez\\views\\templates\\homecategoriez-boilerplate.tpl',
      1 => 1567721665,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '201325d71815dcbb028-21592447',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5d71815e0803f8_32530827',
  'variables' => 
  array (
    'categories' => 0,
    'category' => 0,
    'link' => 0,
    'pic_size_type' => 0,
    'categoryLink' => 0,
    'imageLink' => 0,
    'urls' => 0,
    'language' => 0,
    'pic_size' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d71815e0803f8_32530827')) {function content_5d71815e0803f8_32530827($_smarty_tpl) {?>

<!-- MODULE homecategoriez -->
<div id="homecategoriez">
    <h4 class="section-title">Navegue pelas categorias</h4>
    <ul class="row">
        
        <?php  $_smarty_tpl->tpl_vars['category'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['category']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['categories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['category']->key => $_smarty_tpl->tpl_vars['category']->value) {
$_smarty_tpl->tpl_vars['category']->_loop = true;
?>
            <?php $_smarty_tpl->tpl_vars['categoryLink'] = new Smarty_variable($_smarty_tpl->tpl_vars['link']->value->getcategoryLink($_smarty_tpl->tpl_vars['category']->value->id_category,$_smarty_tpl->tpl_vars['category']->value->link_rewrite), null, 0);?>
            <?php $_smarty_tpl->tpl_vars['imageLink'] = new Smarty_variable($_smarty_tpl->tpl_vars['link']->value->getCatImageLink($_smarty_tpl->tpl_vars['category']->value->link_rewrite,$_smarty_tpl->tpl_vars['category']->value->id_category,$_smarty_tpl->tpl_vars['pic_size_type']->value), null, 0);?>
            <li class="category col-xs-12 col-sm-6 col-lg-3" >
                <a class="box" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['categoryLink']->value, ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['category']->value->name,'html','UTF-8'), ENT_QUOTES, 'UTF-8');?>
" style="background-image: url('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['imageLink']->value, ENT_QUOTES, 'UTF-8');?>
');">
                    <?php if (intval($_smarty_tpl->tpl_vars['category']->value->id_image)>0) {?>
                        <img 
                            class="img-responsive" 
                            src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['imageLink']->value, ENT_QUOTES, 'UTF-8');?>
"
                            
                            
                            alt="<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['category']->value->name,'html','UTF-8'), ENT_QUOTES, 'UTF-8');?>
"
                            title="<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['category']->value->name,'html','UTF-8'), ENT_QUOTES, 'UTF-8');?>
"
                        >
                    <?php } else { ?>
                        <img
                            src="<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['urls']->value['img_cat_url'],'html','UTF-8'), ENT_QUOTES, 'UTF-8');?>
<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['language']->value['iso_code'],'html','UTF-8'), ENT_QUOTES, 'UTF-8');?>
.jpg"
                            width="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pic_size']->value['width'], ENT_QUOTES, 'UTF-8');?>
"
                            height="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pic_size']->value['height'], ENT_QUOTES, 'UTF-8');?>
"
                            title="<?php echo smartyTranslate(array('s'=>'No image','mod'=>'homecategoriez'),$_smarty_tpl);?>
"
                        >
                    <?php }?>
                </a>
                <h5 class="category-title">
                    <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['categoryLink']->value, ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['category']->value->name,'html','UTF-8'), ENT_QUOTES, 'UTF-8');?>
">
                        <?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['category']->value->name,'html','UTF-8'), ENT_QUOTES, 'UTF-8');?>

                    </a>
                </h5>
                
            </li>
        <?php }
if (!$_smarty_tpl->tpl_vars['category']->_loop) {
?>
            <?php echo smartyTranslate(array('s'=>'No categories','mod'=>'homecategoriez'),$_smarty_tpl);?>

        <?php } ?>
    </ul>
</div>
<!-- /MODULE homecategoriez --><?php }} ?>
