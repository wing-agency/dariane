<?php /* Smarty version Smarty-3.1.19, created on 2019-09-05 17:43:43
         compiled from "modules\wtcategoryfeature\views\templates\hook\wtcategoryfeature.tpl" */ ?>
<?php /*%%SmartyHeaderCode:265445d71737f8799b4-54615909%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f6d357f1d3539ff9f8771217222328ca55cfbc9a' => 
    array (
      0 => 'modules\\wtcategoryfeature\\views\\templates\\hook\\wtcategoryfeature.tpl',
      1 => 1567716128,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '265445d71737f8799b4-54615909',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'wt_categories' => 0,
    'item_category' => 0,
    'wtconfig' => 0,
    'category' => 0,
    'link' => 0,
    'i' => 0,
    'nb_sub' => 0,
    'sub_cat' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5d71737faf9e25_74929179',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d71737faf9e25_74929179')) {function content_5d71737faf9e25_74929179($_smarty_tpl) {?>
<div id="wt_category_feature" class="wt_category_feature clearfix">
	<div class="container">
		<div class="title_tab_hide_show">
			
				<?php echo smartyTranslate(array('s'=>'Featured Categories','mod'=>'wtcategoryfeature'),$_smarty_tpl);?>

			
		</div>
		<?php if (isset($_smarty_tpl->tpl_vars['wt_categories']->value)&&count($_smarty_tpl->tpl_vars['wt_categories']->value)>0) {?>
			<div class="list_carousel responsive">
			<a id="wt_cat_prev" class="btn prev" href="#">&lt;</a>
				<a id="wt_cat_next" class="btn next" href="#">&gt;</a>
				<ul id="wt_cat_carousel" class="product-list">
				<?php  $_smarty_tpl->tpl_vars['item_category'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item_category']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['wt_categories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item_category']->key => $_smarty_tpl->tpl_vars['item_category']->value) {
$_smarty_tpl->tpl_vars['item_category']->_loop = true;
?>
				<?php $_smarty_tpl->tpl_vars['category'] = new Smarty_variable($_smarty_tpl->tpl_vars['item_category']->value['category'], null, 0);?>
				
					 <li class="item <?php if (intval($_smarty_tpl->getVariable('smarty')->value['foreach']['item_category']['first'])) {?>first_item<?php } elseif (intval($_smarty_tpl->getVariable('smarty')->value['foreach']['item_category']['last'])) {?>last_item<?php }?>">
						<div class="content">
						<?php if (isset($_smarty_tpl->tpl_vars['wtconfig']->value->showimg)&&$_smarty_tpl->tpl_vars['wtconfig']->value->showimg==1) {?>
						<div class="cat-img">
							<a href="<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['link']->value->getCategoryLink($_smarty_tpl->tpl_vars['category']->value->id_category,$_smarty_tpl->tpl_vars['category']->value->link_rewrite),'html','UTF-8'), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['category']->value->name,'html','UTF-8'), ENT_QUOTES, 'UTF-8');?>
">
								<img src="<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['link']->value->getCatImageLink($_smarty_tpl->tpl_vars['category']->value->link_rewrite,$_smarty_tpl->tpl_vars['category']->value->id_image,'category_default'),'html','UTF-8'), ENT_QUOTES, 'UTF-8');?>
"/>
							</a>
						</div>
						<?php }?>
						<h4 class="title">
							<a href="<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['link']->value->getCategoryLink($_smarty_tpl->tpl_vars['category']->value->id_category,$_smarty_tpl->tpl_vars['category']->value->link_rewrite),'html','UTF-8'), ENT_QUOTES, 'UTF-8');?>
">
								<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['category']->value->name,'html','UTF-8'), ENT_QUOTES, 'UTF-8');?>

							</a>
						</h4>
						<?php if (count($_smarty_tpl->tpl_vars['item_category']->value['sub_cat']>0)&&isset($_smarty_tpl->tpl_vars['wtconfig']->value->showsub)&&$_smarty_tpl->tpl_vars['wtconfig']->value->showsub==1) {?>
						<div class="sub-cat">	
							<ul>
							<?php if (isset($_smarty_tpl->tpl_vars['wtconfig']->value->numbersub)) {?>
								<?php $_smarty_tpl->tpl_vars['nb_sub'] = new Smarty_variable($_smarty_tpl->tpl_vars['wtconfig']->value->numbersub, null, 0);?>
							<?php } else { ?>
								<?php $_smarty_tpl->tpl_vars['nb_sub'] = new Smarty_variable(5, null, 0);?>
							<?php }?>
							<?php $_smarty_tpl->tpl_vars['i'] = new Smarty_variable(0, null, 0);?>
							<?php  $_smarty_tpl->tpl_vars['sub_cat'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['sub_cat']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['item_category']->value['sub_cat']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['sub_cat']->key => $_smarty_tpl->tpl_vars['sub_cat']->value) {
$_smarty_tpl->tpl_vars['sub_cat']->_loop = true;
?>
								<?php $_smarty_tpl->tpl_vars['i'] = new Smarty_variable($_smarty_tpl->tpl_vars['i']->value+1, null, 0);?>
								<?php if ($_smarty_tpl->tpl_vars['i']->value<=$_smarty_tpl->tpl_vars['nb_sub']->value) {?>
								<li>
									<a href="<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['link']->value->getCategoryLink($_smarty_tpl->tpl_vars['sub_cat']->value['id_category'],$_smarty_tpl->tpl_vars['sub_cat']->value['link_rewrite']),'html','UTF-8'), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['sub_cat']->value['name'],'html','UTF-8'), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['sub_cat']->value['name']), ENT_QUOTES, 'UTF-8');?>
</a>
								</li>
								<?php }?>
							<?php } ?>
							</ul>
						</div>
						<?php }?>
						</div>
					 </li>
					
				 <?php } ?>
				 </ul>
			</div>
		<?php } else { ?>
			<p class="alert alert-warning"><?php echo smartyTranslate(array('s'=>'There is no category','mod'=>'wtcategoryfeature'),$_smarty_tpl);?>
</p>
		<?php }?>
		
	</div>
	<?php if (isset($_smarty_tpl->tpl_vars['wtconfig']->value->used_slider)&&$_smarty_tpl->tpl_vars['wtconfig']->value->used_slider==1) {?>
	<script type="text/javascript">
	$(window).load(function() {
		runSliderFeatureCat();
	});

	$(window).resize(function() {
			runSliderFeatureCat();
	});
	
	function runSliderFeatureCat(){
	
	var item_feature_cat = 4;
		
		if(getWidthBrowser() > 1180)
		{	
			item_feature_cat = 4; 
		}
		else
		if(getWidthBrowser() > 991)
		{	
			item_feature_cat = 3; 
		}
		else
		if(getWidthBrowser() > 767)
		{	
			item_feature_cat = 3; 
		}		
		else
		if(getWidthBrowser() > 540)
		{	
			item_feature_cat = 2; 
		}
		else
		if(getWidthBrowser() > 340)
		{	
			item_feature_cat = 2; 
		}			
		
		
			$('#wt_cat_carousel').carouFredSel({
				responsive: true,
				width: '100%',
				height: 'variable',
				onWindowResize: 'debounce',
				prev: '#wt_cat_prev',
				next: '#wt_cat_next',
				auto: false,
				swipe: {
					onTouch : true
				},
				items: {
					width:160,
					height: 'auto',
					visible: {
						min: 2,
						max: item_feature_cat
					}
				},
				scroll: {
					items:2,
					direction : 'left',    
					duration  : 500 ,  
					onBefore: function(data) {  
					},
					onAfter	: function(data) {
					var n=5;
						n=data.items.visible.length;
						$("#carousel1<?php echo htmlspecialchars(intval($_smarty_tpl->getVariable('smarty')->value['foreach']['tabs']['iteration']), ENT_QUOTES, 'UTF-8');?>
 li").removeClass("first_item");
						$("#carousel1<?php echo htmlspecialchars(intval($_smarty_tpl->getVariable('smarty')->value['foreach']['tabs']['iteration']), ENT_QUOTES, 'UTF-8');?>
 li:nth-child(1)").addClass("first_item");
				   }
				}
			});
		
	}
	
</script>
	<?php }?>
</div><?php }} ?>
