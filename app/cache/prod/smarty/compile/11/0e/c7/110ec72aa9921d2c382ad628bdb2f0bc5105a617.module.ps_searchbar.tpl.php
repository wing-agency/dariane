<?php /* Smarty version Smarty-3.1.19, created on 2019-09-09 10:24:14
         compiled from "module:ps_searchbar/ps_searchbar.tpl" */ ?>
<?php /*%%SmartyHeaderCode:78245d7150986b2aa2-01099191%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '110ec72aa9921d2c382ad628bdb2f0bc5105a617' => 
    array (
      0 => 'module:ps_searchbar/ps_searchbar.tpl',
      1 => 1568035358,
      2 => 'module',
    ),
  ),
  'nocache_hash' => '78245d7150986b2aa2-01099191',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5d715098702941_51265702',
  'variables' => 
  array (
    'search_controller_url' => 0,
    'search_string' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5d715098702941_51265702')) {function content_5d715098702941_51265702($_smarty_tpl) {?>
<!-- Block search module TOP -->
<div id="search_widget" class="col-sm-12 col-lg-9 search-widget" data-search-controller-url="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search_controller_url']->value, ENT_QUOTES, 'UTF-8');?>
">
	<div class="desc hidden-lg-down">
		<span>Seja bem-vindo</span>
		<strong>Utilize o campo ao lado para encontrar o que procura</strong>
	</div>
	<form method="get" action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search_controller_url']->value, ENT_QUOTES, 'UTF-8');?>
">
		<input type="hidden" name="controller" value="search">
		<input type="text" name="s" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search_string']->value, ENT_QUOTES, 'UTF-8');?>
" placeholder="O que está procurando? " aria-label="<?php echo smartyTranslate(array('s'=>'Search','d'=>'Shop.Theme.Catalog'),$_smarty_tpl);?>
">
		<button type="submit">
			<i class="material-icons search">&#xE8B6;</i>
      		<span class="hidden-sm-down ">Buscar </span>
		</button>
	</form>
</div>
<!-- /Block search module TOP -->
<?php }} ?>
