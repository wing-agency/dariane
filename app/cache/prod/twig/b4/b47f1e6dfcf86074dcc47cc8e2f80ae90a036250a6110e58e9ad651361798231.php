<?php

/* __string_template__d71f778388b34d267b06042ee429f947decee691cf97663c4aa4f69370114f3b */
class __TwigTemplate_7c3820db58f746e8b8304c48a06483a31ca0fdccc589988782d3710ac3d5a745 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'extra_stylesheets' => array($this, 'block_extra_stylesheets'),
            'content_header' => array($this, 'block_content_header'),
            'content' => array($this, 'block_content'),
            'content_footer' => array($this, 'block_content_footer'),
            'sidebar_right' => array($this, 'block_sidebar_right'),
            'javascripts' => array($this, 'block_javascripts'),
            'extra_javascripts' => array($this, 'block_extra_javascripts'),
            'translate_javascripts' => array($this, 'block_translate_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"br\">
<head>
  <meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=0.75, maximum-scale=0.75, user-scalable=0\">
<meta name=\"apple-mobile-web-app-capable\" content=\"yes\">
<meta name=\"robots\" content=\"NOFOLLOW, NOINDEX\">

<link rel=\"icon\" type=\"image/x-icon\" href=\"/Wing/dariane/img/favicon.ico\" />
<link rel=\"apple-touch-icon\" href=\"/Wing/dariane/img/app_icon.png\" />

<title>Seleção de módulo • Dariane Armarinhos</title>

  <script type=\"text/javascript\">
    var help_class_name = 'AdminModules';
    var iso_user = 'br';
    var lang_is_rtl = '0';
    var full_language_code = 'pt-br';
    var full_cldr_language_code = 'pt-BR';
    var country_iso_code = 'BR';
    var _PS_VERSION_ = '1.7.3.0';
    var roundMode = 2;
    var youEditFieldFor = '';
        var new_order_msg = 'Novo pedido foi feito em sua loja';
    var order_number_msg = 'Número da encomenda: ';
    var total_msg = 'Total: ';
    var from_msg = 'De: ';
    var see_order_msg = 'Ver este pedido';
    var new_customer_msg = 'Um novo cliente se registrou em sua loja';
    var customer_name_msg = 'Nome do cliente: ';
    var new_msg = 'Uma nova nova mensagem foi postada em sua loja.';
    var see_msg = 'Leia esta mensagem';
    var token = '1aa821951f11dfa4a43032514e2b00f8';
    var token_admin_orders = '50a6f5b9ab001b2cd74e86657aeb9bb4';
    var token_admin_customers = 'd62cbf92747d52280b396d81388ee766';
    var token_admin_customer_threads = '900015561c48ebfc96baeb92290b5205';
    var currentIndex = 'index.php?controller=AdminModules';
    var employee_token = '8ae895b1b571a992ca87e5774fdf23d7';
    var choose_language_translate = 'Escolha idioma';
    var default_language = '1';
    var admin_modules_link = '/Wing/dariane/admin3401/index.php/module/catalog/recommended?route=admin_module_catalog_post&_token=v7BTZ6_iQx89LPCLsMkuzkKcmEDAFdIJREICLlltgFA';
    var tab_modules_list = '';
    var update_success_msg = 'Atualizado com sucesso';
    var errorLogin = 'O PrestaShop não conseguiu autenticar-se nos Addons. Por favor, verifique as suas credenciais e sua conexão à internet.';
    var search_product_msg = 'Procure um produto';
  </script>

      <link href=\"/Wing/dariane/admin3401/themes/new-theme/public/theme.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/Wing/dariane/js/jquery/plugins/chosen/jquery.chosen.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/Wing/dariane/admin3401/themes/default/css/vendor/nv.d3.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/Wing/dariane/modules/gamification/views/css/gamification.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/Wing/dariane/js/jquery/plugins/fancybox/jquery.fancybox.css\" rel=\"stylesheet\" type=\"text/css\"/>
  
  <script type=\"text/javascript\">
var baseAdminDir = \"\\/Wing\\/dariane\\/admin3401\\/\";
var baseDir = \"\\/Wing\\/dariane\\/\";
var currency = {\"iso_code\":\"BRL\",\"sign\":\"R\$\",\"name\":\"Real brasileiro\",\"format\":\"\\u00a4#,##0.00\"};
var host_mode = false;
var show_new_customers = \"1\";
var show_new_messages = false;
var show_new_orders = \"1\";
</script>
<script type=\"text/javascript\" src=\"/Wing/dariane/admin3401/themes/new-theme/public/main.bundle.js\"></script>
<script type=\"text/javascript\" src=\"/Wing/dariane/js/jquery/plugins/jquery.chosen.js\"></script>
<script type=\"text/javascript\" src=\"/Wing/dariane/js/admin.js?v=1.7.3.0\"></script>
<script type=\"text/javascript\" src=\"/Wing/dariane/js/cldr.js\"></script>
<script type=\"text/javascript\" src=\"/Wing/dariane/js/tools.js?v=1.7.3.0\"></script>
<script type=\"text/javascript\" src=\"/Wing/dariane/admin3401/public/bundle.js\"></script>
<script type=\"text/javascript\" src=\"/Wing/dariane/js/vendor/d3.v3.min.js\"></script>
<script type=\"text/javascript\" src=\"/Wing/dariane/admin3401/themes/default/js/vendor/nv.d3.min.js\"></script>
<script type=\"text/javascript\" src=\"/Wing/dariane/modules/gamification/views/js/gamification_bt.js\"></script>
<script type=\"text/javascript\" src=\"/Wing/dariane/js/jquery/plugins/fancybox/jquery.fancybox.js\"></script>


  <script>
            var admin_gamification_ajax_url = \"http:\\/\\/localhost\\/Wing\\/dariane\\/admin3401\\/index.php?controller=AdminGamification&token=b499065bd8c4f8cd584786545fb629b4\";
            var current_id_tab = 45;
        </script>

";
        // line 80
        $this->displayBlock('stylesheets', $context, $blocks);
        $this->displayBlock('extra_stylesheets', $context, $blocks);
        echo "</head>
<body class=\"lang-br adminmodules\">



<header id=\"header\">
  <nav id=\"header_infos\" class=\"main-header\">

    <button class=\"btn btn-primary-reverse onclick btn-lg unbind ajax-spinner\"></button>

    
    

    
    <i class=\"material-icons float-left px-1 js-mobile-menu d-md-none\">menu</i>
    <a id=\"header_logo\" class=\"logo float-left\" href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminDashboard&amp;token=d46ca3ca85eb28df7fdb7b10a5d982d1\"></a>

    <div class=\"component d-none d-md-flex\" id=\"quick-access-container\"><div class=\"dropdown quick-accesses\">
  <button class=\"btn btn-link btn-sm dropdown-toggle\" type=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" id=\"quick_select\">
    Acesso Rápido
  </button>
  <div class=\"dropdown-menu\">
          <a class=\"dropdown-item\"
         href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminStats&amp;module=statscheckup&amp;token=a9e3945e9301610664f4d5ada13dc306\"
                 data-item=\"Avaliação do catálogo\"
      >Avaliação do catálogo</a>
          <a class=\"dropdown-item\"
         href=\"http://localhost/Wing/dariane/admin3401/index.php/module/manage?token=1425355cce38b02d50f1f8bcbb4eef0d\"
                 data-item=\"Módulos instalados\"
      >Módulos instalados</a>
          <a class=\"dropdown-item\"
         href=\"http://localhost/Wing/dariane/admin3401/index.php/product/new?token=1425355cce38b02d50f1f8bcbb4eef0d\"
                 data-item=\"New product\"
      >New product</a>
          <a class=\"dropdown-item\"
         href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminCategories&amp;addcategory&amp;token=c636df3da819dbc0a21a974d0a9ca8cf\"
                 data-item=\"Nova Categoria\"
      >Nova Categoria</a>
          <a class=\"dropdown-item\"
         href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminCartRules&amp;addcart_rule&amp;token=d551e30488273eb628d30c2a3ce2b9e6\"
                 data-item=\"Novo Voucher\"
      >Novo Voucher</a>
          <a class=\"dropdown-item\"
         href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminOrders&amp;token=50a6f5b9ab001b2cd74e86657aeb9bb4\"
                 data-item=\"Pedidos\"
      >Pedidos</a>
        <div class=\"dropdown-divider\"></div>
          <a
        class=\"dropdown-item js-quick-link\"
        href=\"#\"
        data-rand=\"200\"
        data-icon=\"icon-AdminParentModulesSf\"
        data-method=\"add\"
        data-url=\"index.php/module/catalog\"
        data-post-link=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminQuickAccesses&token=3e0adf6ae23c14269f9f5d037c22746f\"
        data-prompt-text=\"Atribua um nome a este atalho:\"
        data-link=\" - Lista\"
      >
        <i class=\"material-icons\">add_circle</i>
        Adicionar a página atual ao Acesso Rápido
      </a>
        <a class=\"dropdown-item\" href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminQuickAccesses&token=3e0adf6ae23c14269f9f5d037c22746f\">
      <i class=\"material-icons\">settings</i>
      Gerir atalhos
    </a>
  </div>
</div>
</div>
    <div class=\"component d-none d-md-inline-block col-md-4\" id=\"header-search-container\">
<form id=\"header_search\"
      class=\"bo_search_form dropdown-form js-dropdown-form collapsed\"
      method=\"post\"
      action=\"/Wing/dariane/admin3401/index.php?controller=AdminSearch&amp;token=3b5188579ad7b65071a752b538aa355c\"
      role=\"search\">
  <input type=\"hidden\" name=\"bo_search_type\" id=\"bo_search_type\" class=\"js-search-type\" />
    <div class=\"input-group\">
    <input type=\"text\" class=\"form-control js-form-search\" id=\"bo_query\" name=\"bo_query\" value=\"\" placeholder=\"Pesquisar (por ex.: referência do produto, nome do cliente…)\">
    <div class=\"input-group-btn\">
      <button type=\"button\" class=\"btn btn-outline-secondary dropdown-toggle js-dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
        em todo lugar
      </button>
      <div class=\"dropdown-menu js-items-list\">
        <a class=\"dropdown-item\" data-item=\"em todo lugar\" href=\"#\" data-value=\"0\" data-placeholder=\"O que está procurando?\" data-icon=\"icon-search\"><i class=\"material-icons\">search</i> em todo lugar</a>
        <div class=\"dropdown-divider\"></div>
        <a class=\"dropdown-item\" data-item=\"Catálogo\" href=\"#\" data-value=\"1\" data-placeholder=\"Nome do produto, SKU, referência...\" data-icon=\"icon-book\"><i class=\"material-icons\">store_mall_directory</i> Catálogo</a>
        <a class=\"dropdown-item\" data-item=\"Clientes Nome\" href=\"#\" data-value=\"2\" data-placeholder=\"E-mail, nome...\" data-icon=\"icon-group\"><i class=\"material-icons\">group</i> Clientes Nome</a>
        <a class=\"dropdown-item\" data-item=\"Clientes por endereço ip\" href=\"#\" data-value=\"6\" data-placeholder=\"123.45.67.89\" data-icon=\"icon-desktop\"><i class=\"material-icons\">desktop_mac</i> Clientes por endereço IP</a>
        <a class=\"dropdown-item\" data-item=\"Pedidos\" href=\"#\" data-value=\"3\" data-placeholder=\"ID do pedido\" data-icon=\"icon-credit-card\"><i class=\"material-icons\">shopping_basket</i> Pedidos</a>
        <a class=\"dropdown-item\" data-item=\"Faturas\" href=\"#\" data-value=\"4\" data-placeholder=\"Número da fatura\" data-icon=\"icon-book\"><i class=\"material-icons\">book</i></i> Faturas</a>
        <a class=\"dropdown-item\" data-item=\"Carrinhos de Compras\" href=\"#\" data-value=\"5\" data-placeholder=\"ID do Carrinho\" data-icon=\"icon-shopping-cart\"><i class=\"material-icons\">shopping_cart</i> Carrinhos de Compras</a>
        <a class=\"dropdown-item\" data-item=\"Módulos\" href=\"#\" data-value=\"7\" data-placeholder=\"Nome do módulo\" data-icon=\"icon-puzzle-piece\"><i class=\"material-icons\">extension</i> Módulos</a>
      </div>
      <button class=\"btn btn-primary\" type=\"submit\"><span class=\"d-none\">PESQUISAR</span><i class=\"material-icons\">search</i></button>
    </div>
  </div>
</form>

<script type=\"text/javascript\">
 \$(document).ready(function(){
    \$('#bo_query').one('click', function() {
    \$(this).closest('form').removeClass('collapsed');
  });
});
</script>
</div>

            <div class=\"component d-none d-md-inline-block\">  <div class=\"shop-list\">
    <a class=\"link\" id=\"header_shopname\" href=\"http://localhost/Wing/dariane/\" target= \"_blank\">Dariane Armarinhos</a>
  </div>
</div>
          <div class=\"component\"><div id=\"notif\" class=\"notification-center dropdown dropdown-clickable\">
  <div class=\"notification js-notification dropdown-toggle\" data-toggle=\"dropdown\">
    <i class=\"material-icons\">notifications_none</i>
    <span id=\"notifications-total\" class=\"count hide\">0</span>
  </div>
  <div class=\"dropdown-menu dropdown-menu-right js-notifs_dropdown\">
    <div class=\"notifications\">
      <ul class=\"nav nav-tabs\" role=\"tablist\">
                          <li class=\"nav-item\">
            <a
              class=\"nav-link active\"
              id=\"orders-tab\"
              data-toggle=\"tab\"
              data-type=\"order\"
              href=\"#orders-notifications\"
              role=\"tab\"
            >
              Encomendas<span id=\"_nb_new_orders_\"></span>
            </a>
          </li>
                                    <li class=\"nav-item\">
            <a
              class=\"nav-link \"
              id=\"customers-tab\"
              data-toggle=\"tab\"
              data-type=\"customer\"
              href=\"#customers-notifications\"
              role=\"tab\"
            >
              Clientes<span id=\"_nb_new_customers_\"></span>
            </a>
          </li>
                                    <li class=\"nav-item\">
            <a
              class=\"nav-link \"
              id=\"messages-tab\"
              data-toggle=\"tab\"
              data-type=\"customer_message\"
              href=\"#messages-notifications\"
              role=\"tab\"
            >
              Mensagens<span id=\"_nb_new_messages_\"></span>
            </a>
          </li>
                        </ul>

      <!-- Tab panes -->
      <div class=\"tab-content\">
                          <div class=\"tab-pane active empty\" id=\"orders-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              Nenhuma nova encomenda por enquanto :(<br>
              Have you checked your <strong><a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminCarts&token=58244e212ee14601ffbb7ab23183b047&action=filterOnlyAbandonedCarts\">abandoned carts</a></strong>?<br>Your next order could be hiding there!
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                                    <div class=\"tab-pane  empty\" id=\"customers-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              Nenhum novo cliente por enquanto :(<br>
              Tem estado ativo nas redes sociais estes últimos dias?
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                                    <div class=\"tab-pane  empty\" id=\"messages-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              Sem nova mensagem por enquanto.<br>
              As más notícias correm depressa, não é?
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                        </div>
    </div>
  </div>
</div>

  <script type=\"text/html\" id=\"order-notification-template\">
    <a class=\"notif\" href='order_url'>
      #_id_order_ -
      de <strong>_customer_name_</strong> (_iso_code_)_carrier_
      <strong class=\"float-sm-right\">_total_paid_</strong>
    </a>
  </script>

  <script type=\"text/html\" id=\"customer-notification-template\">
    <a class=\"notif\" href='customer_url'>
      #_id_customer_ - <strong>_customer_name_</strong>_company_ - registado <strong>_date_add_</strong>
    </a>
  </script>

  <script type=\"text/html\" id=\"message-notification-template\">
    <a class=\"notif\" href='message_url'>
    <span class=\"message-notification-status _status_\">
      <i class=\"material-icons\">fiber_manual_record</i> _status_
    </span>
      - <strong>_customer_name_</strong> (_company_) - <i class=\"material-icons\">access_time</i> _date_add_
    </a>
  </script>
</div>
        <div class=\"component -norightmargin d-none d-md-inline-block\"><div class=\"employee-dropdown dropdown\">
      <div class=\"rounded-circle person\" data-toggle=\"dropdown\">
      <i class=\"material-icons\">person</i>
    </div>
    <div class=\"dropdown-menu dropdown-menu-right\">
    <div class=\"text-xs-center employee_avatar\">
      <img class=\"avatar rounded-circle\" src=\"http://profile.prestashop.com/wingweb%40agenciawing.com.br.jpg\" /><br>
      <span>Wing Agency</span>
    </div>
    <div>
      <a class=\"employee-link profile-link\" href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminEmployees&amp;token=8ae895b1b571a992ca87e5774fdf23d7&amp;id_employee=1&amp;updateemployee\">
        <i class=\"material-icons\">settings_applications</i> O seu perfil
      </a>
    </div>
    <div>
      <a class=\"employee-link\" id=\"header_logout\" href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminLogin&amp;token=bbfc3b4c371c90479fe6eaf8c424b0da&amp;logout\">
        <i class=\"material-icons\">power_settings_new</i> Sair
      </a>
    </div>
  </div>
</div>
</div>

    
  </nav>
  </header>

<nav class=\"nav-bar\">
  <ul class=\"main-menu\">

          
                
                
        
          <li class=\"link-levelone \" data-submenu=\"1\" id=\"tab-AdminDashboard\">
            <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminDashboard&amp;token=d46ca3ca85eb28df7fdb7b10a5d982d1\" class=\"link\" >
              <i class=\"material-icons\">trending_up</i> <span>Painel</span>
            </a>
          </li>

        
                
                                  
                
        
          <li class=\"category-title d-none d-sm-block \" data-submenu=\"2\" id=\"tab-SELL\">
              <span class=\"title\">Vender</span>
          </li>

                          
                
                                
                <li class=\"link-levelone \" data-submenu=\"3\" id=\"subtab-AdminParentOrders\">
                  <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminOrders&amp;token=50a6f5b9ab001b2cd74e86657aeb9bb4\" class=\"link\">
                    <i class=\"material-icons\">shopping_basket</i>
                    <span>
                    Pedidos
                                          <i class=\"material-icons float-right d-md-none\">keyboard_arrow_down</i>
                                        </span>

                  </a>
                                          <ul id=\"collapse-3\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"4\" id=\"subtab-AdminOrders\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminOrders&amp;token=50a6f5b9ab001b2cd74e86657aeb9bb4\" class=\"link\"> Pedidos
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"5\" id=\"subtab-AdminInvoices\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminInvoices&amp;token=b7ee6de5e86091b356b44cce2f2e8345\" class=\"link\"> Faturas
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"6\" id=\"subtab-AdminSlip\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminSlip&amp;token=fef51174d6b68384da56fd280e07b965\" class=\"link\"> Comprovantes de créditos
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"7\" id=\"subtab-AdminDeliverySlip\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminDeliverySlip&amp;token=5fe09dd5e6123cf560f1ede7206b9515\" class=\"link\"> Comprovantes de entregas
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"8\" id=\"subtab-AdminCarts\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminCarts&amp;token=58244e212ee14601ffbb7ab23183b047\" class=\"link\"> Carrinho de compras
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"9\" id=\"subtab-AdminCatalog\">
                  <a href=\"/Wing/dariane/admin3401/index.php/product/catalog?_token=v7BTZ6_iQx89LPCLsMkuzkKcmEDAFdIJREICLlltgFA\" class=\"link\">
                    <i class=\"material-icons\">store</i>
                    <span>
                    Catálogo
                                          <i class=\"material-icons float-right d-md-none\">keyboard_arrow_down</i>
                                        </span>

                  </a>
                                          <ul id=\"collapse-9\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"10\" id=\"subtab-AdminProducts\">
                              <a href=\"/Wing/dariane/admin3401/index.php/product/catalog?_token=v7BTZ6_iQx89LPCLsMkuzkKcmEDAFdIJREICLlltgFA\" class=\"link\"> Produtos
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"11\" id=\"subtab-AdminCategories\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminCategories&amp;token=c636df3da819dbc0a21a974d0a9ca8cf\" class=\"link\"> Categorias
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"12\" id=\"subtab-AdminTracking\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminTracking&amp;token=61a5d657f4759d3f176a574f79912473\" class=\"link\"> Monitoramento
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"13\" id=\"subtab-AdminParentAttributesGroups\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminAttributesGroups&amp;token=b295228741bec306e60723776e9df21a\" class=\"link\"> Atributos e Características
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"16\" id=\"subtab-AdminParentManufacturers\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminManufacturers&amp;token=85bdc2bf6548d3945817fd058d649c2f\" class=\"link\"> Marcas e Fornecedores
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"19\" id=\"subtab-AdminAttachments\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminAttachments&amp;token=2dfce107c01291b4e803dd9748c3ae82\" class=\"link\"> Arquivos
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"20\" id=\"subtab-AdminParentCartRules\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminCartRules&amp;token=d551e30488273eb628d30c2a3ce2b9e6\" class=\"link\"> Descontos
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"23\" id=\"subtab-AdminStockManagement\">
                              <a href=\"/Wing/dariane/admin3401/index.php/stock/?_token=v7BTZ6_iQx89LPCLsMkuzkKcmEDAFdIJREICLlltgFA\" class=\"link\"> Stocks
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"24\" id=\"subtab-AdminParentCustomer\">
                  <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminCustomers&amp;token=d62cbf92747d52280b396d81388ee766\" class=\"link\">
                    <i class=\"material-icons\">account_circle</i>
                    <span>
                    Clientes
                                          <i class=\"material-icons float-right d-md-none\">keyboard_arrow_down</i>
                                        </span>

                  </a>
                                          <ul id=\"collapse-24\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"25\" id=\"subtab-AdminCustomers\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminCustomers&amp;token=d62cbf92747d52280b396d81388ee766\" class=\"link\"> Clientes
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"26\" id=\"subtab-AdminAddresses\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminAddresses&amp;token=c53e956790bd67c99cc042bc11f0a59e\" class=\"link\"> Endereços
                              </a>
                            </li>

                                                                                                                          </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"28\" id=\"subtab-AdminParentCustomerThreads\">
                  <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminCustomerThreads&amp;token=900015561c48ebfc96baeb92290b5205\" class=\"link\">
                    <i class=\"material-icons\">chat</i>
                    <span>
                    Atendimento ao Cliente
                                          <i class=\"material-icons float-right d-md-none\">keyboard_arrow_down</i>
                                        </span>

                  </a>
                                          <ul id=\"collapse-28\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"29\" id=\"subtab-AdminCustomerThreads\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminCustomerThreads&amp;token=900015561c48ebfc96baeb92290b5205\" class=\"link\"> Atendimento ao Cliente
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"30\" id=\"subtab-AdminOrderMessage\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminOrderMessage&amp;token=19cc53bbe4d4dc7a2b4cfe73b410f65a\" class=\"link\"> Mensagens de pedidos
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"31\" id=\"subtab-AdminReturn\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminReturn&amp;token=df33df550bef09a54bbb8529fd53e0ee\" class=\"link\"> Devoluções de Mercadorias
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"32\" id=\"subtab-AdminStats\">
                  <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminStats&amp;token=a9e3945e9301610664f4d5ada13dc306\" class=\"link\">
                    <i class=\"material-icons\">assessment</i>
                    <span>
                    Estatísticas
                                        </span>

                  </a>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title d-none d-sm-block -active\" data-submenu=\"42\" id=\"tab-IMPROVE\">
              <span class=\"title\">Melhorar</span>
          </li>

                          
                
                                
                <li class=\"link-levelone -active\" data-submenu=\"43\" id=\"subtab-AdminParentModulesSf\">
                  <a href=\"/Wing/dariane/admin3401/index.php/module/catalog?_token=v7BTZ6_iQx89LPCLsMkuzkKcmEDAFdIJREICLlltgFA\" class=\"link\">
                    <i class=\"material-icons\">extension</i>
                    <span>
                    Módulos
                                          <i class=\"material-icons float-right d-md-none\">keyboard_arrow_down</i>
                                        </span>

                  </a>
                                          <ul id=\"collapse-43\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"44\" id=\"subtab-AdminModulesSf\">
                              <a href=\"/Wing/dariane/admin3401/index.php/module/catalog?_token=v7BTZ6_iQx89LPCLsMkuzkKcmEDAFdIJREICLlltgFA\" class=\"link\"> Módulos e serviços
                              </a>
                            </li>

                                                                                                                              
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"46\" id=\"subtab-AdminAddonsCatalog\">
                              <a href=\"/Wing/dariane/admin3401/index.php/module/addons-store?_token=v7BTZ6_iQx89LPCLsMkuzkKcmEDAFdIJREICLlltgFA\" class=\"link\"> Catálogo de Módulos
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"47\" id=\"subtab-AdminParentThemes\">
                  <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminThemes&amp;token=24ee5aa32dd6bec1d93e8f4fafecce02\" class=\"link\">
                    <i class=\"material-icons\">desktop_mac</i>
                    <span>
                    Design
                                          <i class=\"material-icons float-right d-md-none\">keyboard_arrow_down</i>
                                        </span>

                  </a>
                                          <ul id=\"collapse-47\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"48\" id=\"subtab-AdminThemes\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminThemes&amp;token=24ee5aa32dd6bec1d93e8f4fafecce02\" class=\"link\"> Tema Gráfico e Logótipo
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"49\" id=\"subtab-AdminThemesCatalog\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminThemesCatalog&amp;token=dc8927202b99f7c24efc7629f14dd241\" class=\"link\"> Catálogo do Tema Gráfico
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"50\" id=\"subtab-AdminCmsContent\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminCmsContent&amp;token=512f6457e7f162168d4b2fdea1fc53cd\" class=\"link\"> Páginas
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"51\" id=\"subtab-AdminModulesPositions\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminModulesPositions&amp;token=af0244c9bda43661737a7cb1329a2ca4\" class=\"link\"> Posições
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"52\" id=\"subtab-AdminImages\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminImages&amp;token=a32ff6a52aac240b095df444339a5faa\" class=\"link\"> Definições de Imagem
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"117\" id=\"subtab-AdminLinkWidget\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminLinkWidget&amp;token=066074fa955832b0b073f9b704b24f80\" class=\"link\"> Link Widget
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"53\" id=\"subtab-AdminParentShipping\">
                  <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminCarriers&amp;token=d9f6874426481f3d223735b80d78bc96\" class=\"link\">
                    <i class=\"material-icons\">local_shipping</i>
                    <span>
                    Frete
                                          <i class=\"material-icons float-right d-md-none\">keyboard_arrow_down</i>
                                        </span>

                  </a>
                                          <ul id=\"collapse-53\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"54\" id=\"subtab-AdminCarriers\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminCarriers&amp;token=d9f6874426481f3d223735b80d78bc96\" class=\"link\"> Transportadoras
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"55\" id=\"subtab-AdminShipping\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminShipping&amp;token=ce9cf43cfe52b0b799b92c2f8ecff041\" class=\"link\"> Preferências
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"56\" id=\"subtab-AdminParentPayment\">
                  <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminPayment&amp;token=b04dbb87321088ba9c92087080676a81\" class=\"link\">
                    <i class=\"material-icons\">payment</i>
                    <span>
                    Pagamento
                                          <i class=\"material-icons float-right d-md-none\">keyboard_arrow_down</i>
                                        </span>

                  </a>
                                          <ul id=\"collapse-56\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"57\" id=\"subtab-AdminPayment\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminPayment&amp;token=b04dbb87321088ba9c92087080676a81\" class=\"link\"> Métodos de Pagamento
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"58\" id=\"subtab-AdminPaymentPreferences\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminPaymentPreferences&amp;token=4eed68cbce8bda2977f339ba886647f0\" class=\"link\"> Preferências
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"59\" id=\"subtab-AdminInternational\">
                  <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminLocalization&amp;token=ffa3e7edf726a46ce9116dd43bd188dc\" class=\"link\">
                    <i class=\"material-icons\">language</i>
                    <span>
                    Internacional
                                          <i class=\"material-icons float-right d-md-none\">keyboard_arrow_down</i>
                                        </span>

                  </a>
                                          <ul id=\"collapse-59\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"60\" id=\"subtab-AdminParentLocalization\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminLocalization&amp;token=ffa3e7edf726a46ce9116dd43bd188dc\" class=\"link\"> Localização
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"65\" id=\"subtab-AdminParentCountries\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminZones&amp;token=110e699e921b1772076f9a3f7902a7f5\" class=\"link\"> Localizações
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"69\" id=\"subtab-AdminParentTaxes\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminTaxes&amp;token=69e8aca888fc1b32723dd2046080d378\" class=\"link\"> Taxas
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"72\" id=\"subtab-AdminTranslations\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminTranslations&amp;token=5b3bf21a2b7f0dcc0b6d0f9b43cd54b9\" class=\"link\"> Traduções
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title d-none d-sm-block \" data-submenu=\"73\" id=\"tab-CONFIGURE\">
              <span class=\"title\">Configurar</span>
          </li>

                          
                
                                
                <li class=\"link-levelone \" data-submenu=\"74\" id=\"subtab-ShopParameters\">
                  <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminPreferences&amp;token=80189f9a017a88e87fb8abec18e572bd\" class=\"link\">
                    <i class=\"material-icons\">settings</i>
                    <span>
                    Parâmetros da Loja
                                          <i class=\"material-icons float-right d-md-none\">keyboard_arrow_down</i>
                                        </span>

                  </a>
                                          <ul id=\"collapse-74\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"75\" id=\"subtab-AdminParentPreferences\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminPreferences&amp;token=80189f9a017a88e87fb8abec18e572bd\" class=\"link\"> Geral
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"78\" id=\"subtab-AdminParentOrderPreferences\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminOrderPreferences&amp;token=363193b0fdbc8ed33180253860720d2c\" class=\"link\"> Definições da Encomenda
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"81\" id=\"subtab-AdminPPreferences\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminPPreferences&amp;token=214fff6ff095aeefe9ba3390b30c1661\" class=\"link\"> Produtos
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"82\" id=\"subtab-AdminParentCustomerPreferences\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminCustomerPreferences&amp;token=053b6122841c36365f8ef824eeebd907\" class=\"link\"> Customer Settings
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"86\" id=\"subtab-AdminParentStores\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminContacts&amp;token=173dc353dcac6dc1c8547c67a98146db\" class=\"link\"> Contato
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"89\" id=\"subtab-AdminParentMeta\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminMeta&amp;token=3ada4ec5a50f7659a2da03096e91f3e1\" class=\"link\"> Tráfego e SEO
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"93\" id=\"subtab-AdminParentSearchConf\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminSearchConf&amp;token=75699f9404d306021db29bcf51e5cbe8\" class=\"link\"> Busca
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"119\" id=\"subtab-AdminGamification\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminGamification&amp;token=b499065bd8c4f8cd584786545fb629b4\" class=\"link\"> Merchant Expertise
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"96\" id=\"subtab-AdminAdvancedParameters\">
                  <a href=\"/Wing/dariane/admin3401/index.php/configure/advanced/system_information?_token=v7BTZ6_iQx89LPCLsMkuzkKcmEDAFdIJREICLlltgFA\" class=\"link\">
                    <i class=\"material-icons\">settings_applications</i>
                    <span>
                    Parâmetros Avançados
                                          <i class=\"material-icons float-right d-md-none\">keyboard_arrow_down</i>
                                        </span>

                  </a>
                                          <ul id=\"collapse-96\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"97\" id=\"subtab-AdminInformation\">
                              <a href=\"/Wing/dariane/admin3401/index.php/configure/advanced/system_information?_token=v7BTZ6_iQx89LPCLsMkuzkKcmEDAFdIJREICLlltgFA\" class=\"link\"> Informação
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"98\" id=\"subtab-AdminPerformance\">
                              <a href=\"/Wing/dariane/admin3401/index.php/configure/advanced/performance?_token=v7BTZ6_iQx89LPCLsMkuzkKcmEDAFdIJREICLlltgFA\" class=\"link\"> Desempenho
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"99\" id=\"subtab-AdminAdminPreferences\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminAdminPreferences&amp;token=8998bcf57e02e8711f2b71644147ca63\" class=\"link\"> Administração
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"100\" id=\"subtab-AdminEmails\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminEmails&amp;token=b2325e4320ac04bb8c422840f886d82c\" class=\"link\"> Email
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"101\" id=\"subtab-AdminImport\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminImport&amp;token=d6e29d08ca06013f67933773e80d761c\" class=\"link\"> Importar
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"102\" id=\"subtab-AdminParentEmployees\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminEmployees&amp;token=8ae895b1b571a992ca87e5774fdf23d7\" class=\"link\"> Team
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"106\" id=\"subtab-AdminParentRequestSql\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminRequestSql&amp;token=f6f9ab11c23cff92e9ecc2368c7a8eb0\" class=\"link\"> Base de dados
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"109\" id=\"subtab-AdminLogs\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminLogs&amp;token=e465ef1c8fb5fb2312647925dac01724\" class=\"link\"> Logs
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"110\" id=\"subtab-AdminWebservice\">
                              <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminWebservice&amp;token=64b83f748177e340297840b381285556\" class=\"link\"> WebService
                              </a>
                            </li>

                                                                                                                                                                            </ul>
                                    </li>
                          
        
            </ul>

  <span class=\"menu-collapse d-none d-md-block\">
    <i class=\"material-icons\">&#xE8EE;</i>
  </span>

  
</nav>


<div id=\"main-div\">

  
    
<div class=\"header-toolbar\">

  
    <nav class=\"breadcrumb\">

                        <a class=\"breadcrumb-item\" href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminParentModulesSf&amp;token=76768ea75f3176f98bb2b4fad5273048\">Módulos</a>
              
      
    </nav>
  

  
    <h2 class=\"title\">
      Seleção de módulo    </h2>
  

  
    <div class=\"toolbar-icons\">
      
                        
          <a
            class=\"mx-1 btn btn-primary  pointer\"            id=\"page-header-desc-configuration-add_module\"
            href=\"#\"            title=\"Enviar um módulo\"            data-toggle=\"pstooltip\"
            data-placement=\"bottom\"          >
            <i class=\"material-icons\">cloud_upload</i>
            <span class=\"title\">Enviar um módulo</span>
          </a>
                                
          <a
            class=\"mx-1 btn btn-primary  pointer\"            id=\"page-header-desc-configuration-addons_connect\"
            href=\"#\"            title=\"Conectar ao Marketplace Addons\"            data-toggle=\"pstooltip\"
            data-placement=\"bottom\"          >
            <i class=\"material-icons\">vpn_key</i>
            <span class=\"title\">Conectar ao Marketplace Addons</span>
          </a>
                          
                  <a class=\"toolbar-button btn-help btn-sidebar\" href=\"#\"
             title=\"Ajuda\"
             data-toggle=\"sidebar\"
             data-target=\"#right-sidebar\"
             data-url=\"/Wing/dariane/admin3401/index.php/common/sidebar/http%253A%252F%252Fhelp.prestashop.com%252Fbr%252Fdoc%252FAdminModules%253Fversion%253D1.7.3.0%2526country%253Dbr/Ajuda?_token=v7BTZ6_iQx89LPCLsMkuzkKcmEDAFdIJREICLlltgFA\"
             id=\"product_form_open_help\"
          >
            <i class=\"material-icons\">help</i>
            <span class=\"title\">Ajuda</span>
          </a>
                  </div>
  
        <div class=\"page-head-tabs\" id=\"head_tabs\">
                <a class=\"tab current\"
   href=\"/Wing/dariane/admin3401/index.php/module/catalog?_token=v7BTZ6_iQx89LPCLsMkuzkKcmEDAFdIJREICLlltgFA\">Seleção</a>

                <a class=\"tab\"
   href=\"/Wing/dariane/admin3401/index.php/module/manage?_token=v7BTZ6_iQx89LPCLsMkuzkKcmEDAFdIJREICLlltgFA\">Módulos instalados</a>

                <a class=\"tab\"
   href=\"/Wing/dariane/admin3401/index.php/module/notifications?_token=v7BTZ6_iQx89LPCLsMkuzkKcmEDAFdIJREICLlltgFA\">Notificações  <div class=\"notification-container\">
    <span class=\"notification-counter\">13</span>
  </div>
  </a>

            </div>
      
</div>
    <div class=\"modal fade\" id=\"modal_addons_connect\" tabindex=\"-1\">
\t<div class=\"modal-dialog modal-md\">
\t\t<div class=\"modal-content\">
\t\t\t\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>
\t\t\t\t<h4 class=\"modal-title\"><i class=\"icon-puzzle-piece\"></i> <a target=\"_blank\" href=\"http://addons.prestashop.com/?utm_source=back-office&utm_medium=modules&utm_campaign=back-office-BR&utm_content=download\">PrestaShop Addons</a></h4>
\t\t\t</div>
\t\t\t
\t\t\t
<div class=\"modal-body\">
\t\t\t\t\t\t<!--start addons login-->
\t\t\t<form id=\"addons_login_form\" method=\"post\" >
\t\t\t\t<div>
\t\t\t\t\t<a href=\"https://addons.prestashop.com/br/login?email=wingweb%40agenciawing.com.br&amp;firstname=Wing&amp;lastname=Agency&amp;website=http%3A%2F%2Flocalhost%2FWing%2Fdariane%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-BR&amp;utm_content=download#createnow\"><img class=\"img-responsive center-block\" src=\"/Wing/dariane/admin3401/themes/default/img/prestashop-addons-logo.png\" alt=\"Logo PrestaShop Addons\"/></a>
\t\t\t\t\t<h3 class=\"text-center\">Ligue a sua loja ao mercado PrestaShop para poder importar automaticamente todas as suas compras Addons.</h3>
\t\t\t\t\t<hr />
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Não tem uma conta?</h4>
\t\t\t\t\t\t<p class='text-justify'>Descubra as vantagens do PrestaShop Addons! Explore o MarketPlace Oficial do PrestaShop e encontre mais de 3.500 módulos inovadores e temas que ajudam a otimizar as taxas de conversão, aumentar o tráfego na loja, desenvolver a fidelidade dos clientes e maximizar a sua produtividade</p>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Conectar ao PrestaShop Addons</h4>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><i class=\"icon-user\"></i></span>
\t\t\t\t\t\t\t\t<input id=\"username_addons\" name=\"username_addons\" type=\"text\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><i class=\"icon-key\"></i></span>
\t\t\t\t\t\t\t\t<input id=\"password_addons\" name=\"password_addons\" type=\"password\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<a class=\"btn btn-link float-right _blank\" href=\"//addons.prestashop.com/br/forgot-your-password\">Esqueci minha senha</a>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"row row-padding-top\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<a class=\"btn btn-default btn-block btn-lg _blank\" href=\"https://addons.prestashop.com/br/login?email=wingweb%40agenciawing.com.br&amp;firstname=Wing&amp;lastname=Agency&amp;website=http%3A%2F%2Flocalhost%2FWing%2Fdariane%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-BR&amp;utm_content=download#createnow\">
\t\t\t\t\t\t\t\tCriar uma conta
\t\t\t\t\t\t\t\t<i class=\"icon-external-link\"></i>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<button id=\"addons_login_button\" class=\"btn btn-primary btn-block btn-lg\" type=\"submit\">
\t\t\t\t\t\t\t\t<i class=\"icon-unlock\"></i> Entrar
\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div id=\"addons_loading\" class=\"help-block\"></div>

\t\t\t</form>
\t\t\t<!--end addons login-->
\t\t\t</div>


\t\t\t\t\t</div>
\t</div>
</div>

    <div class=\"content-div \">

      

      

      

      
      
      
      

      <div class=\"row \">
        <div class=\"col-sm-12\">
          <div id=\"ajax_confirmation\" class=\"alert alert-success\" style=\"display: none;\"></div>




  ";
        // line 1111
        $this->displayBlock('content_header', $context, $blocks);
        // line 1112
        echo "                 ";
        $this->displayBlock('content', $context, $blocks);
        // line 1113
        echo "                 ";
        $this->displayBlock('content_footer', $context, $blocks);
        // line 1114
        echo "                 ";
        $this->displayBlock('sidebar_right', $context, $blocks);
        // line 1115
        echo "
        </div>
      </div>

    </div>

  
</div>

<div id=\"non-responsive\" class=\"js-non-responsive\">
  <h1>Oh, não!</h1>
  <p class=\"mt-3\">
    A versão móvel desta página ainda não está disponível.
  </p>
  <p class=\"mt-2\">
    Utilize um computador para obter acesso a esta página, até ser adaptada para um telemóvel.
  </p>
  <p class=\"mt-2\">
    Obrigado.
  </p>
  <a href=\"http://localhost/Wing/dariane/admin3401/index.php?controller=AdminDashboard&amp;token=d46ca3ca85eb28df7fdb7b10a5d982d1\" class=\"btn btn-primary py-1 mt-3\">
    <i class=\"material-icons\">arrow_back</i>
    Voltar
  </a>
</div>
<div class=\"mobile-layer\"></div>



  <div id=\"footer\" class=\"bootstrap\">
    
</div>



  <div class=\"bootstrap\">
    <div class=\"modal fade\" id=\"modal_addons_connect\" tabindex=\"-1\">
\t<div class=\"modal-dialog modal-md\">
\t\t<div class=\"modal-content\">
\t\t\t\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>
\t\t\t\t<h4 class=\"modal-title\"><i class=\"icon-puzzle-piece\"></i> <a target=\"_blank\" href=\"http://addons.prestashop.com/?utm_source=back-office&utm_medium=modules&utm_campaign=back-office-BR&utm_content=download\">PrestaShop Addons</a></h4>
\t\t\t</div>
\t\t\t
\t\t\t
<div class=\"modal-body\">
\t\t\t\t\t\t<!--start addons login-->
\t\t\t<form id=\"addons_login_form\" method=\"post\" >
\t\t\t\t<div>
\t\t\t\t\t<a href=\"https://addons.prestashop.com/br/login?email=wingweb%40agenciawing.com.br&amp;firstname=Wing&amp;lastname=Agency&amp;website=http%3A%2F%2Flocalhost%2FWing%2Fdariane%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-BR&amp;utm_content=download#createnow\"><img class=\"img-responsive center-block\" src=\"/Wing/dariane/admin3401/themes/default/img/prestashop-addons-logo.png\" alt=\"Logo PrestaShop Addons\"/></a>
\t\t\t\t\t<h3 class=\"text-center\">Ligue a sua loja ao mercado PrestaShop para poder importar automaticamente todas as suas compras Addons.</h3>
\t\t\t\t\t<hr />
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Não tem uma conta?</h4>
\t\t\t\t\t\t<p class='text-justify'>Descubra as vantagens do PrestaShop Addons! Explore o MarketPlace Oficial do PrestaShop e encontre mais de 3.500 módulos inovadores e temas que ajudam a otimizar as taxas de conversão, aumentar o tráfego na loja, desenvolver a fidelidade dos clientes e maximizar a sua produtividade</p>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Conectar ao PrestaShop Addons</h4>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><i class=\"icon-user\"></i></span>
\t\t\t\t\t\t\t\t<input id=\"username_addons\" name=\"username_addons\" type=\"text\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><i class=\"icon-key\"></i></span>
\t\t\t\t\t\t\t\t<input id=\"password_addons\" name=\"password_addons\" type=\"password\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<a class=\"btn btn-link float-right _blank\" href=\"//addons.prestashop.com/br/forgot-your-password\">Esqueci minha senha</a>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"row row-padding-top\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<a class=\"btn btn-default btn-block btn-lg _blank\" href=\"https://addons.prestashop.com/br/login?email=wingweb%40agenciawing.com.br&amp;firstname=Wing&amp;lastname=Agency&amp;website=http%3A%2F%2Flocalhost%2FWing%2Fdariane%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-BR&amp;utm_content=download#createnow\">
\t\t\t\t\t\t\t\tCriar uma conta
\t\t\t\t\t\t\t\t<i class=\"icon-external-link\"></i>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<button id=\"addons_login_button\" class=\"btn btn-primary btn-block btn-lg\" type=\"submit\">
\t\t\t\t\t\t\t\t<i class=\"icon-unlock\"></i> Entrar
\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div id=\"addons_loading\" class=\"help-block\"></div>

\t\t\t</form>
\t\t\t<!--end addons login-->
\t\t\t</div>


\t\t\t\t\t</div>
\t</div>
</div>

  </div>

";
        // line 1223
        $this->displayBlock('javascripts', $context, $blocks);
        $this->displayBlock('extra_javascripts', $context, $blocks);
        $this->displayBlock('translate_javascripts', $context, $blocks);
        echo "</body>
</html>";
    }

    // line 80
    public function block_stylesheets($context, array $blocks = array())
    {
    }

    public function block_extra_stylesheets($context, array $blocks = array())
    {
    }

    // line 1111
    public function block_content_header($context, array $blocks = array())
    {
    }

    // line 1112
    public function block_content($context, array $blocks = array())
    {
    }

    // line 1113
    public function block_content_footer($context, array $blocks = array())
    {
    }

    // line 1114
    public function block_sidebar_right($context, array $blocks = array())
    {
    }

    // line 1223
    public function block_javascripts($context, array $blocks = array())
    {
    }

    public function block_extra_javascripts($context, array $blocks = array())
    {
    }

    public function block_translate_javascripts($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "__string_template__d71f778388b34d267b06042ee429f947decee691cf97663c4aa4f69370114f3b";
    }

    public function getDebugInfo()
    {
        return array (  1302 => 1223,  1297 => 1114,  1292 => 1113,  1287 => 1112,  1282 => 1111,  1273 => 80,  1265 => 1223,  1155 => 1115,  1152 => 1114,  1149 => 1113,  1146 => 1112,  1144 => 1111,  109 => 80,  28 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "__string_template__d71f778388b34d267b06042ee429f947decee691cf97663c4aa4f69370114f3b", "");
    }
}
